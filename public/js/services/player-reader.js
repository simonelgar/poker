
filePath = '../../public/storage/players.json';

function jsonReader(filePath, cb) {
    
    const fs = require('fs');
    
    fs.readFile(filePath, (err, fileData) => {
        if (err) {
            return cb && cb(err);
        }
        try {
            const object = JSON.parse(fileData);
            return cb && cb(null, object);
        } catch(err) {
            return cb && cb(err);
        }
    })
}

function writeNewPlayer(player) {

    const fs = require('fs');
    const jsonString = JSON.stringify(player);

    fs.appendFile(filePath, jsonString, err => {
        if (err) {
            console.log('Error writing file', err);
        } else {
            console.log('Successfully wrote file');
        }
    })
}

function getPlayersFromFile() {
    
    const fs = require('fs');
    
    fs.readFile(filePath, (err, fileData) => {
        
        const object = JSON.parse(fileData);
        return object;
        
    })
}