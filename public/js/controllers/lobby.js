app.controller('LobbyController', ['$scope', '$rootScope', '$http', '$window', '$cookies', function( $scope, $rootScope, $http, $window, $cookies ) {
	$scope.lobbyTables = [];
	$scope.newScreenName = '';

	$http({
		url: '/lobby-data',
		method: 'GET'
	}).success(function ( data, status, headers, config ) {
		for( tableId in data ) {
			$scope.lobbyTables[tableId] = data[tableId];
		}
	});

	// Want to check if the user has a cookie
	if ($cookies.user_id) { 
		// User has a cookie
		socket.emit( 'register', $cookies.user_id, true, function( response ){
			if( response.success ){
				// The user therefore should get registered and this will disable the html... 
				$rootScope.screenName = response.screenName;
				$rootScope.totalChips = response.totalChips;
				$scope.registerError = '';
				$rootScope.$digest();
			}
			$scope.$digest();
		});
	}

	$scope.register = function() {
		// If there is some trimmed value for a new screen name
		if( $scope.newScreenName ) {
			socket.emit( 'register', $scope.newScreenName, false, function( response ){
				if( response.success ){
					$rootScope.screenName = response.screenName;
					$rootScope.totalChips = response.totalChips;
					$scope.registerError = '';
					$cookies.user_id = response.screenName; // Assign that cookie
					$rootScope.$digest();
				}
				else if( response.message ) {
					$scope.registerError = response.message;
				}
				$scope.$digest();
			});
		}
	}


	}]);
